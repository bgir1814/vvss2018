package repository;

import evaluator.controller.IntrebareController;
import evaluator.model.Intrebare;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;
import org.junit.jupiter.api.BeforeAll;

import java.util.List;

/**
 * Created by Gabriel1210 on 5/9/2018.
 */
class CreeazaTestTest {
    static IntrebareController intrebareController;

    @BeforeAll
    static void init() {
        intrebareController = new IntrebareController();
    }

    @org.junit.jupiter.api.Test
    void TC01_WBT() {
        try {
            intrebareController.loadIntrebariFromFile("intrebari.txt");
            Test t = intrebareController.createNewTest();
            assert(true);
        } catch (Exception ex){
            System.out.println("TC01_WBT -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    void TC02_WBT() {
        String enunt = "Mere?";
        String raspuns1 = "1)Mere";
        String raspuns2 = "2)Mere";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            intrebareController.addNewIntrebare(enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, categoria);
            Test t = intrebareController.createNewTest();
            assert(false);
        } catch (Exception ex){
            System.out.println("TC02_WBT -> " + ex.getMessage());
            assert(true);
        }
    }

}
