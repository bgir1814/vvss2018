package repository;

import evaluator.controller.IntrebareController;
import evaluator.model.IntrebareStatistica;
import evaluator.model.Test;
import org.junit.jupiter.api.BeforeEach;

/**
 * Created by Gabriel1210 on 5/9/2018.
 */
public class BigBangTest {
    static IntrebareController intrebareController;

    @BeforeEach
    void init() {
        intrebareController = new IntrebareController();
    }

    @org.junit.jupiter.api.Test
    public void P_A(){
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Bucuresti";
        String raspuns2 = "2)Barcelona";
        String raspuns3 = "3)Pitest";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            intrebareController.addNewIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("P_A -> intrebare adaugata");
            assert(true);
        } catch (Exception ex) {
            System.out.println("P_A -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    public void P_B(){
        try {
            intrebareController.loadIntrebariFromFile("intrebari.txt");
            Test test = intrebareController.createNewTest();
            assert(true);
        } catch (Exception ex){
            System.out.println("P_B -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    public void P_C(){
        intrebareController.loadIntrebariFromFile("intrebari.txt");
        try{
            IntrebareStatistica intsts = intrebareController.getStatistica();
            assert(true);
        }catch (Exception ex){
            System.out.println("P_C -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    public void P_A_B_C(){
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Bucuresti";
        String raspuns2 = "2)Barcelona";
        String raspuns3 = "3)Pitest";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            intrebareController.loadIntrebariFromFile("intrebari.txt");
            intrebareController.addNewIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            Test t = intrebareController.createNewTest();
            assert(t.getIntrebari().size() > 0);
            IntrebareStatistica sts = intrebareController.getStatistica();
            assert(sts.getIntrebariDomenii().size() > 0);
            assert(true);
        } catch (Exception ex) {
            System.out.println("P_A_B_C -> " + ex.getMessage());
            assert(false);
        }
    }
}
