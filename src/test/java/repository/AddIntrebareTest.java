package repository;

import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.jupiter.api.BeforeEach;

/**
 * Created by Gabriel1210 on 5/9/2018.
 */

import static org.junit.jupiter.api.Assertions.*;

class AddIntrebareTest {

    static IntrebariRepository intrebariRepository;

    @BeforeEach
    void init() {
        intrebariRepository = new IntrebariRepository();
    }
    void addIntrebare(String enunt, String raspuns1, String raspuns2, String raspuns3, String raspunsCorect, String categoria) throws Exception{
        Intrebare intrebare = new Intrebare(enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, categoria);
        intrebariRepository.addIntrebare(intrebare);
    }

    @org.junit.jupiter.api.Test
    void TC01_EP() {
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Bucuresti";
        String raspuns2 = "2)Barcelona";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC01_EP -> intrebare adaugata");
            assert(true);
        } catch (Exception ex) {
            System.out.println("TC01_EP -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    void TC03_EP() {
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Bucuresti";
        String raspuns2 = "2)Barcelona";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "asd";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC03_EP -> intrebare adaugata");
            assert(false);
        } catch (Exception ex) {
            System.out.println("TC03_EP -> " + ex.getMessage());
            assert(true);
        }
    }

    @org.junit.jupiter.api.Test
    void TC04_EP() {
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Bucuresti";
        String raspuns2 = "Mere";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC04_EP -> intrebare adaugata");
            assert(false);
        } catch (Exception ex) {
            System.out.println("TC04_EP -> " + ex.getMessage());
            assert(true);
        }
    }

    @org.junit.jupiter.api.Test
    void TC05_EP() {
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "Mere";
        String raspuns2 = "2)Barcelona";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC05_EP -> intrebare adaugata");
            assert(false);
        } catch (Exception ex) {
            System.out.println("TC05_EP -> " + ex.getMessage());
            assert(true);
        }
    }

    @org.junit.jupiter.api.Test
    void TC07_EP() {
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Bucuresti";
        String raspuns2 = "2)Barcelona";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "4";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC07_EP -> intrebare adaugata");
            assert(false);
        } catch (Exception ex) {
            System.out.println("TC07_EP -> " + ex.getMessage());
            assert(true);
        }
    }

}
