package repository;

import evaluator.controller.IntrebareController;
import evaluator.model.IntrebareStatistica;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

/**
 * Created by Gabriel1210 on 5/9/2018.
 */
public class StatisticiTest {
    static IntrebareController intrebareController;

    @BeforeEach
    void init() {
        intrebareController = new IntrebareController();
    }

    @org.junit.jupiter.api.Test
    void TC01_WBT() {
        intrebareController.loadIntrebariFromFile("intrebari.txt");
        try{
            IntrebareStatistica is = intrebareController.getStatistica();
            assert(true);
        }catch (Exception ex){
            System.out.println("TC02_WBT -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    void TC02_WBT() {
        try{
            IntrebareStatistica is = intrebareController.getStatistica();
            assert(false);
        }catch (Exception ex){
            System.out.println("TC02_WBT -> " + ex.getMessage());
            assert(true);
        }
    }
}
