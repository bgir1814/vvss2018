package repository;

import evaluator.controller.IntrebareController;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class IntrebariRepositoryTest {

    static IntrebariRepository intrebariRepository;

    @BeforeEach
    void init() {
        intrebariRepository = new IntrebariRepository();
    }
    void addIntrebare(String enunt, String raspuns1, String raspuns2, String raspuns3, String raspunsCorect, String categoria) throws Exception{
        Intrebare intrebare = new Intrebare(enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, categoria);
        intrebariRepository.addIntrebare(intrebare);
    }

    @org.junit.jupiter.api.Test
    void TC01_EP() {
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Bucuresti";
        String raspuns2 = "2)Barcelona";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC01_EP -> intrebare adaugata");
            assert(true);
        } catch (Exception ex) {
            System.out.println("TC01_EP -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    void TC03_EP() {
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Bucuresti";
        String raspuns2 = "2)Barcelona";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "asd";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC03_EP -> intrebare adaugata");
            assert(false);
        } catch (Exception ex) {
            System.out.println("TC03_EP -> " + ex.getMessage());
            assert(true);
        }
    }

    @org.junit.jupiter.api.Test
    void TC04_EP() {
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Bucuresti";
        String raspuns2 = "Mere";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC04_EP -> intrebare adaugata");
            assert(false);
        } catch (Exception ex) {
            System.out.println("TC04_EP -> " + ex.getMessage());
            assert(true);
        }
    }

    @org.junit.jupiter.api.Test
    void TC05_EP() {
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "Mere";
        String raspuns2 = "2)Barcelona";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC05_EP -> intrebare adaugata");
            assert(false);
        } catch (Exception ex) {
            System.out.println("TC05_EP -> " + ex.getMessage());
            assert(true);
        }
    }

    @org.junit.jupiter.api.Test
    void TC07_EP() {
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Bucuresti";
        String raspuns2 = "2)Barcelona";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "4";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC07_EP -> intrebare adaugata");
            assert(false);
        } catch (Exception ex) {
            System.out.println("TC07_EP -> " + ex.getMessage());
            assert(true);
        }
    }

    @org.junit.jupiter.api.Test
    void TC02_BVA() {
        String enunt = "Mere";
        String raspuns1 = "1)Mere";
        String raspuns2 = "2)Mere";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Categorie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC02_BVA -> intrebare adaugata");
            assert(false);
        } catch (Exception ex) {
            System.out.println("TC02_BVA -> " + ex.getMessage());
            assert(true);
        }
    }

    @org.junit.jupiter.api.Test
    void TC05_BVA() {
        String enunt = "Mere?";
        String raspuns1 = "1)Mere";
        String raspuns2 = "2)Mere";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Categorie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC05_BVA -> intrebare adaugata");
            assert(true);
        } catch (Exception ex) {
            System.out.println("TC05_BVA -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    void TC08_BVA() {
        String enunt = "Mere?";
        String raspuns1 = "Mere";
        String raspuns2 = "2)Mere";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Categorie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC08_BVA -> intrebare adaugata");
            assert(false);
        } catch (Exception ex) {
            System.out.println("TC08_BVA -> " + ex.getMessage());
            assert(true);
        }
    }

    @org.junit.jupiter.api.Test
    void TC15_BVA() {
        String enunt = "Mere?";
        String raspuns1 = "1)Mere";
        String raspuns2 = "2)Mere";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "";
        String categoria = "Categorie";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC15_BVA -> intrebare adaugata");
            assert(false);
        } catch (Exception ex) {
            System.out.println("TC15_BVA -> " + ex.getMessage());
            assert(true);
        }
    }

    @org.junit.jupiter.api.Test
    void TC22_BVA() {
        String enunt = "Mere?";
        String raspuns1 = "1)Mere";
        String raspuns2 = "2)Mere";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Mere";
        try {
            addIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("TC22_BVA -> Intrebari empty");
            assert(true);
        } catch (Exception ex) {
            System.out.println("TC15_BVA -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    void TC01_WBT() {
        List<Intrebare> l = intrebariRepository.getIntrebariByDomain("Geografie");
        assert (l.isEmpty());
        System.out.println("TC01_WBT -> succes");
    }

    @org.junit.jupiter.api.Test
    void TC02_WBT() {
        String enunt = "Capitala Romaniei ?";
        String raspuns1 = "1)Mere";
        String raspuns2 = "2)Mere";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Mere";
        try {
            addIntrebare(enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, categoria);
            List<Intrebare> l = intrebariRepository.getIntrebariByDomain("Geografie");
            assert (intrebariRepository.getIntrebari().size() == 1);
            assert (l.size() == 0);
            System.out.println("TC02_WBT -> Intrebari not empty, domeniu negasit");
        } catch (Exception ex){
            System.out.println("TC02_WBT -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    void TC03_WBT() {
        String enunt = "Mere?";
        String raspuns1 = "1)Mere";
        String raspuns2 = "2)Mere";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, categoria);
            List<Intrebare> l = intrebariRepository.getIntrebariByDomain("Geografie");
            assert (intrebariRepository.getIntrebari().size() == 1);
            assert (l.size() == 1);
            System.out.println("TC03_WBT -> Intrebari not empty, domeniu gasit");
        } catch (Exception ex){
            System.out.println("TC03_WBT -> " + ex.getMessage());
            assert(false);
        }
    }

    // Lab4
    @org.junit.jupiter.api.Test
    void TC04_WBT() {
        String enunt = "Mere?";
        String raspuns1 = "1)Mere";
        String raspuns2 = "2)Mere";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, categoria);
            List<Intrebare> l = intrebariRepository.getIntrebariByDomain("Geografie");
            assert (intrebariRepository.getIntrebari().size() == 1);
            assert (l.size() == 1);
            System.out.println("TC03_WBT -> Intrebari not empty, domeniu gasit");
        } catch (Exception ex){
            System.out.println("TC03_WBT -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    void TC05_WBT() {
        String enunt = "Mere?";
        String raspuns1 = "1)Mere";
        String raspuns2 = "2)Mere";
        String raspuns3 = "3)Pitesti";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            addIntrebare(enunt, raspuns1, raspuns2, raspuns3, raspunsCorect, categoria);
            List<Intrebare> l = intrebariRepository.getIntrebariByDomain("Geografie");
            assert (intrebariRepository.getIntrebari().size() == 1);
            assert (l.size() == 1);
            System.out.println("TC03_WBT -> Intrebari not empty, domeniu gasit");
        } catch (Exception ex){
            System.out.println("TC03_WBT -> " + ex.getMessage());
            assert(false);
        }
    }

}