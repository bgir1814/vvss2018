package repository;

import evaluator.controller.IntrebareController;
import evaluator.model.IntrebareStatistica;
import evaluator.model.Test;
import org.junit.jupiter.api.BeforeEach;

/**
 * Created by Gabriel1210 on 5/9/2018.
 */
public class TopDownTest {

    static IntrebareController intrebareController;

    @BeforeEach
    void init() {
        intrebareController = new IntrebareController();
    }

    @org.junit.jupiter.api.Test
    public void Unit_A(){
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Bucuresti";
        String raspuns2 = "2)Madrid";
        String raspuns3 = "3)Pitest";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            intrebareController.addNewIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            System.out.println("Unit_A -> intrebare adaugata");
            assert(true);
        } catch (Exception ex) {
            System.out.println("Unit_A -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    public void P_A_B(){
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Cluj";
        String raspuns2 = "2)Satu Mare";
        String raspuns3 = "3)Pitest";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            intrebareController.loadIntrebariFromFile("intrebari.txt");
            intrebareController.addNewIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            Test t = intrebareController.createNewTest();
            assert(t.getIntrebari().size() > 0);
            assert(true);
        } catch (Exception ex) {
            System.out.println("P_A_B -> " + ex.getMessage());
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    public void P_A_B_C(){
        String enunt = "Care este capitala Romaniei?";
        String raspuns1 = "1)Cluj";
        String raspuns2 = "2)Targiu Jiu";
        String raspuns3 = "3)Pitest";
        String raspunsCorect = "1";
        String categoria = "Geografie";
        try {
            intrebareController.loadIntrebariFromFile("intrebari.txt");
            intrebareController.addNewIntrebare(enunt,raspuns1,raspuns2,raspuns3,raspunsCorect,categoria);
            Test t = intrebareController.createNewTest();
            IntrebareStatistica sts = intrebareController.getStatistica();
            assert(sts.getIntrebariDomenii().size() > 0);
            assert(t.getIntrebari().size() > 0);
            assert(true);
        } catch (Exception ex) {
            System.out.println("P_A_B_C -> " + ex.getMessage());
            assert(false);
        }
    }
}
