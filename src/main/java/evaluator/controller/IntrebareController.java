package evaluator.controller;

import java.util.LinkedList;
import java.util.List;

import evaluator.exception.IntrebareValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.model.IntrebareStatistica;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.NotAbleToCreateIntrebareStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;

public class IntrebareController {

	private IntrebariRepository intrebariRepository;

	public IntrebareController() {
		intrebariRepository = new IntrebariRepository();
	}

	public Intrebare addNewIntrebare(String enunt, String varianta1, String varianta2, String varianta3,
                                     String variantaCorecta, String domeniu) throws DuplicateIntrebareException, IntrebareValidationFailedException {


        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        intrebariRepository.addIntrebare(intrebare);
        return intrebare;
    }

	public boolean exists(Intrebare intrebare) {
		return intrebariRepository.exists(intrebare);
	}

	public Test createNewTest() throws NotAbleToCreateTestException {

		if (intrebariRepository.getIntrebari().size() < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente intrebari pentru crearea unui test!(5)");

		if (intrebariRepository.getNumberOfDistinctDomains() < 5)
			throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");

		List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
		List<String> domenii = new LinkedList<String>();
		Intrebare intrebare;
		Test test = new Test();

		while (testIntrebari.size() != 5) {
			intrebare = intrebariRepository.pickRandomIntrebare();

			if (!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())) {
				testIntrebari.add(intrebare);
				domenii.add(intrebare.getDomeniu());
			}

		}

		test.setIntrebari(testIntrebari);
		return test;

	}

	public void loadIntrebariFromFile(String f) {
		intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile(f));
	}

	public IntrebareStatistica getStatistica() throws NotAbleToCreateIntrebareStatisticsException {

		if (intrebariRepository.getIntrebari().isEmpty())
			throw new NotAbleToCreateIntrebareStatisticsException("Repository-ul nu contine nicio intrebare!");

		IntrebareStatistica intrebareStatistica = new IntrebareStatistica();
		for (String domeniu : intrebariRepository.getDistinctDomains()) {
			intrebareStatistica.add(domeniu, intrebariRepository.getNumberOfIntrebariByDomain(domeniu));
		}

		return intrebareStatistica;
	}

}
