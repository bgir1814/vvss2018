package evaluator.exception;

public class NotAbleToCreateIntrebareStatisticsException extends Exception {

	private static final long serialVersionUID = 1L;

	public NotAbleToCreateIntrebareStatisticsException(String message) {
		super(message);
	}

}
