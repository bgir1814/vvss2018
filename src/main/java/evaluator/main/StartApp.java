package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import evaluator.model.Intrebare;
import evaluator.model.IntrebareStatistica;

import evaluator.controller.IntrebareController;
import evaluator.exception.NotAbleToCreateIntrebareStatisticsException;
import evaluator.model.Test;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

    private static final String file = "intrebari.txt";

    public static void main(String[] args) throws IOException {

        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        Scanner in = new Scanner(System.in);

        IntrebareController intrebareController = new IntrebareController();

        intrebareController.loadIntrebariFromFile(file);

        boolean activ = true;
        String optiune = null;

        while (activ) {

            System.out.println("");
            System.out.println("1.Adauga intrebare");
            System.out.println("2.Creeaza test");
            System.out.println("3.IntrebareStatistica");
            System.out.println("4.Exit");
            System.out.println("");

            optiune = console.readLine();

            switch (optiune) {
                case "1":
                    String domeniu, enunt, varianta1, varianta2, varianta3, variantaCorecta;

                    // Domeniul
                    System.out.println("Domeniu:");
                    domeniu = in.nextLine();

                    // Enuntul
                    System.out.println("Enunt:");
                    enunt = in.nextLine();

                    // Varianta1
                    System.out.println("Varianta1:");
                    varianta1 = in.nextLine();

                    // Varianta2
                    System.out.println("Varianta2:");
                    varianta2 = in.nextLine();

                    // Varianta3
                    System.out.println("Varianta3:");
                    varianta3 = in.nextLine();

                    // Varianta corecta
                    System.out.println("Varianta corecta:");
                    variantaCorecta = in.nextLine();

                    try {
                        intrebareController.addNewIntrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "2":
                    try {
                        Test test = intrebareController.createNewTest();
                        for (Intrebare i : test.getIntrebari()) {
                            System.out.println(i);
                        }
                        System.out.println();
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                    break;
                case "3":
                    IntrebareStatistica intrebareStatistica;
                    try {
                        intrebareStatistica = intrebareController.getStatistica();
                        System.out.println(intrebareStatistica);
                    } catch (NotAbleToCreateIntrebareStatisticsException e) {
                        System.out.println("Nu pot sa creez statistica!");
                    }

                    break;
                case "4":
                    activ = false;
                    break;
                default:
                    break;
            }
        }
    }
}
